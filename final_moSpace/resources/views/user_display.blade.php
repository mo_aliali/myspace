@extends('layouts.master')

@section('title', 'user_display')

@section('content')
    <?php

    $users = \App\User::get();

    ?>

    <div class="container">
        <h1>Users:</h1>
        @foreach($users as $user)
            <div>
                <p><strong>Data:</strong></p> <strong>Username:</strong> {{ $user->username }}  <br> <strong>Profile picture:</strong>{{ $user->image }}
            </div>
            @if (Storage::disk('local')->has($user->first_name . '-' . $user->id . '.jpg'))
                <section class="row new-post">
                    <div class="col-md-6 col-md-offset-3">
                        <img class="image_intervention" src="{{ route('account.image', ['filename' => $user->first_name . '-' . $user->id . '.jpg']) }}" alt="" class="img-responsive">
                    </div>
                </section>
            @endif
        @endforeach
    </div>

@endsection
<!--http://localhost/php/projects/MoSpace/public/?page=user&id=1-->