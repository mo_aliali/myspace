@extends('layouts.master')

@section('title')
    Account
@endsection

@section('content')
    {{--<link rel="stylesheet" type="text/css" href="public/src.main,css">--}}
    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>Your Account</h3></header>
            <form action="{{ route('account.save') }}" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}" id="first_name">
                </div>
                <div class="form-group">
                    <label for="first_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}" id="last_name">
                </div>
                <div class="form-group">
                    <label for="first_name">Username</label>
                    <input type="text" name="username" class="form-control" value="{{ $user->username }}" id="username">
                </div>
                <div class="form-group">
                    <label for="first_name">Address</label>
                    <input type="text" name="address" class="form-control" value="{{ $user->address }}" id="address">
                </div>
                <div class="form-group">
                    <label for="first_name">Relation status</label>
                    <input type="text" name="relation_status" class="form-control" value="{{ $user->relation_status }}" id="relation_status">
                </div>

                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file" name="image" class="form-control" id="image">
                </div>
                <button type="submit" class="btn btn-primary">Save Account</button>
                <input type="hidden" value="{{ Session::token() }}" name="_token">
            </form>
        </div>
    </section>
    @if (Storage::disk('local')->has($user->first_name . '-' . $user->id . '.jpg'))
        <section class="row new-post">
            <div class="col-md-6 col-md-offset-3">
                <img class="image_intervention" src="{{ route('account.image', ['filename' => $user->first_name . '-' . $user->id . '.jpg']) }}" alt="" class="img-responsive">
            </div>
        </section>
    @endif
@endsection